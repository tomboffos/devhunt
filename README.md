# Job Finding

<div class="row">
          <div class="col-12">
            <div class="row card-wrap" style="box-shadow: none">
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/01_Splash.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/02_Onboarding 01.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/03_Onboarding 02.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/04_Onboarding 02.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/05_Login.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/06_Register.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/07_Home.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/09_Search.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/10_Search Filter.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/12_Job Details_Description.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/13_Job Details_Requirements.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/14_Job Details_Company.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/16_Job Details_Write Feedback.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/17_Apply Form.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/19_Apply Complete.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/20_Applications_Applied.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/21_Applications_Interviews.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/22_Messages.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/23_Message_Person.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/25_Account.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/27_Notification Setting.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/28_Notification.png"
                  alt=""
                />
              </div>
              <div class="col-md-4 col-1 screen-item">
                <img
                  style="
                    width: 20%;
                    height: 20%;
                    box-shadow: 0 0 6px #8e8e8e;
                    border-radius: 10px;
                    overflow: hidden;
                  "
                  src="screenshots/29_Setting.png"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>