import 'package:job_finding/modules/message/model/user_model.dart';

const List<UserModel> chatList = <UserModel>[
  UserModel(
      id: 0,
      unSeenMsg: 2,
      avatar: 'https://picsum.photos/id/237/200/200',
      name: "Omar Faruk",
      lastMeg: "hello lastMeg",
      time: "2019-07-19 8:40:23"),
  UserModel(
      id: 1,
      unSeenMsg: 0,
      avatar: 'https://picsum.photos/id/23/200/200',
      name: "Shahi",
      lastMeg: "hello shahi this is your lastMeg",
      time: "2019-07-19 8:40:24"),
  UserModel(
      id: 2,
      unSeenMsg: 0,
      avatar: 'https://picsum.photos/id/27/200/200',
      name: "Mamun",
      lastMeg: "hello omar faruk this is your lastMeg",
      time: "2019-07-19 8:40:25"),
  UserModel(
      id: 3,
      unSeenMsg: 1,
      avatar: 'https://picsum.photos/id/7/200/200',
      name: "Omar Faruk",
      lastMeg: "hello lastMeg",
      time: "2019-07-19 8:40:23"),
  UserModel(
      id: 4,
      unSeenMsg: 0,
      avatar: 'https://picsum.photos/id/3/200/200',
      name: "Shahi",
      lastMeg: "hello shahi this is your lastMeg",
      time: "2019-07-19 8:40:24"),
  UserModel(
      id: 5,
      unSeenMsg: 0,
      avatar: 'https://picsum.photos/id/2/200/200',
      name: "Mamun",
      lastMeg: "hello omar faruk this is your lastMeg",
      time: "2019-07-19 8:40:25"),
];
