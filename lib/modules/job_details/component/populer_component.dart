import 'package:flutter/material.dart';
import 'package:job_finding/utils/constants.dart';

class PopulerComponent extends StatelessWidget {
  const PopulerComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Text(
        """
Uniquely incubate alternative relationships and holistic manufactured products. Competently iterate distributed technologies via multimedia based markets. Progressively incubate granular bandwidth with bleeding-edge sources.

Dynamically integrate virtual growth strategies through seamless internal or "organic" sources. multimedia based markets. Progressively incubate granular bandwidth with bleeding-edge sources.

strategies through seamless internal or "organic" sources. multimedia based markets.
""",
        style: TextStyle(color: paragraphColor),
      ),
    );
  }
}
