import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFFffffff);
const Color blackColor = Color(0xff040404);
const Color fillColor = Color(0xffFAFAFA);
const Color boarderColor = Color(0xffe5e4e4);
const Color labelColor = Color(0xff939393);
const Color secondaryColor = Color(0xff4FAA89);
const Color suffixColor = Color(0xff686873);
const Color redColor = Color(0xffFC5E53);
const Color circleColor = Color(0xffc4c4c4);
const Color navSelectedColor = Color(0xff130F26);
const Color paragraphColor = Color(0xff777676);
const Color textColor = Color(0xff2C2C2C);
const Color captionTextColor = Color(0xff777676);
const Color skillsColorOne = Color(0xffFDECD2);
const Color skillsColorTwo = Color(0xffD1FAEB);
const Color skillsColorThree = Color(0xffEBE2FF);
