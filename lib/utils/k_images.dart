class Kimages {
  static const String homeIcon = "assets/icons/home.svg";
  static const String applicationIcon = "assets/icons/application.svg";
  static const String navIndicateIcon = "assets/icons/navIndicate.svg";
  static const String messageIcon = "assets/icons/message.svg";
  static const String congrateIcon = "assets/icons/congrate.svg";
  static const String videoIcon = "assets/icons/video.svg";
  static const String notificationIcon = "assets/icons/notification.svg";
  static const String congratulationsIcon = "assets/icons/congratulations.svg";
  static const String downloadIcon = "assets/icons/download.svg";
  static const String drawerIcon = "assets/icons/drawer.svg";
  static const String editIcon = "assets/icons/edit.svg";
  static const String figmaIcon = "assets/icons/figma.svg";
  static const String logoutIcon = "assets/icons/logout.svg";
  static const String photoshopIcon = "assets/icons/photoshop.svg";
  static const String pixencyIcon = "assets/icons/pixency.svg";
  static const String searchIcon = "assets/icons/search.svg";
  static const String filterIcon = "assets/icons/filter.svg";
  static const String sendIcon = "assets/icons/send.svg";
  static const String lineIcon = "assets/icons/line.svg";
  static const String shopifyIcon = "assets/icons/shopify.svg";
  static const String uiLeadIcon = "assets/icons/ui_lead.svg";
  static const String xdIcon = "assets/icons/xd.svg";
  static const String onboarding_1 = "assets/images/onboarding_1.png";
  static const String onboarding_2 = "assets/images/onboarding_2.png";
  static const String onboardingBottom = "assets/icons/on_boarding_bottom.svg";
  static const String logoIcon = "assets/images/logo.svg";
  static const String logoBlackIcon = "assets/images/logo_black.svg";
  static const String noInterviewImage = "assets/images/no_interview.png";

  static const String arrowVector = 'assets/icons/arrow_vector.svg';
  static const String arrowWhite = 'assets/icons/arrow_white.svg';
  static const String arrowBlack = 'assets/icons/arrow_black.svg';
}
